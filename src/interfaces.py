# -*- coding: iso-8859-1 -*-


# Interfaces in Form von abstrakten Methoden.
# Definieren, welche Form gewisse Klassen haben sollen, insbesondere die Schnittstelle zwischen GUI und Model
# soll hier definiert werden.
# Implementiert via package 'abc'
# siehe: http://pymotw.com/2/abc/

# Die implementierende klasse erbt von der abstrakten Klasse und kann folgenden Check verwenden:
##if __name__ == '__main__':
	##print 'Subclass:', issubclass(SubclassImplementation, PluginBase)
	##print 'Instance:', isinstance(SubclassImplementation(), PluginBase)

# NOTE: Interpreter beschwert sich, falls nicht alle Methoden der abstrakten Klasse implementiert wurden!


import abc

class InputOutputAbstract(object):
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def select_hand_cards(self):
		"""return a list of valid hand-card-indices."""
		return n

	@abc.abstractmethod
	def display_card(self,card): ### deprecated
		"""Show a single card, give a card (value,color). No return value."""
		return

	@abc.abstractmethod
	def display_cards(self,cards): ### deprecated
		"""Show a multiple card, give a list of cards [(value,color)]. No return value."""
		return

	@abc.abstractmethod
	def display_top_card(self,open_cards): ### deprecated
		"""Show a multiple card, give a list of cards [(value,color)]. No return value."""
		return
	
	@abc.abstractmethod
	def publish_current_state(self):
		"""Send a list of lists as representation of the current state of the game to the gui."""
		return
