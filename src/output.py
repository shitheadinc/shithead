# -*- coding: iso-8859-1 -*-

import sys
from interfaces import InputOutputAbstract

class PrototypeGUI(InputOutputAbstract):

	def __init__(self):
		## diese abfragen check ob die abstrakte Klasse als Vorlage verwendet wurde
		if __name__ == '__main__':
			if not issubclass(PrototypeGUI, InputOutputAbstract):
				print("ERROR: Abstract class not subclassed!")
			if not isinstance(PrototypeGUI(), InputOutputAbstract):
				print("ERROR: Abstract class not instanciated!")
				
		# Für Ausgabe:
		self.print_color = {1:"d",2:"h",3:"s",4:"c"}
		self.print_value = {1:"2",2:"3",3:"4",4:"5",5:"6",6:"7",7:"8", \
			8:"9",9:"Ten",10:"Jok",11:"Que",12:"Kin",13:"Ace"}
		return

	############### INPUT ##################
	# lasse den user einen input machen, um karten abzuwerfen
	def select_hand_cards(self):
		n = []
		inp = raw_input(' Choose a valid card or type -1 to take all cards (if possible): ')
		try:
			n = [int(i) for i in inp.split()] # In Zahl umwandeln
		except:
			raise ValueError('Retry!') # falls Keine Zahl, Schleife abbrechen und neu starten
		return n


	############### OUTPUT ##################
	def display_card(self,c):
		value = self.print_value[c[0]] # Wert der Karte
		suit = self.print_color[c[1]] # Farbe der Karte
		return value + '-' + suit
		
	# Gibt die Hand des Spielers aus:
	def display_cards(self,cards):
		disp = ""
		i = 0
		for c in cards:
			disp += (" ["+str(i)+"] ")
			disp += self.display_card(c)
			#disp += "\n"
			i += 1
		print(disp + '\n'),

	# Gibt die oberste Karte aus:
	def display_top_card(self,open_cards):
		if not open_cards:
			print("\n Stack is empty, please play a card")
		else:
			topstr = "\n Current top card: "
			topstr += self.display_card(open_cards[-1])
			print(topstr)
