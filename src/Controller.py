from view.MainWindow import *
from view.Constants import *

#from game import *

class Controller:
	def __init__(self):
		self.win = MainWindow(self.on_event)
		self.win.place_card("Qc",145,275)
		self.win.place_card("Kc",145,275+y_offset)
		self.win.show_all()
		self.win.place_player_hand(["2d","2c","2h","4s"])
		self.win.place_villain_hand(13)
		self.win.place_selector()
		Gtk.main()
		#self.model = Game()
	
	def on_event(self,widget,data):
		name = widget.get_name()
		print "Event empfangen von %s" % name
		if name=="NEW_GAME":
			#TODO: fill with content
			print "Neues Spiel gestartet."
		elif name=="STAGE":
			self.on_stage_clicked(data)
	
	# Wird mit der Maus auf die Stage geklickt, dann bewegt sich
	# die Kreuz-Dame an die Position des Mauszeigers
	# Zusaetzlich Ausgabe der Klick-Position und der Stage-Groesse	
	def on_stage_clicked(self,data):
		print "Stage clicked at (%.2f, %.2f)" % (data.x, data.y)
		print "Stage size: (%.2f, %.2f)" % (self.win.stage.get_width(),
			self.win.stage.get_height())
		self.win.move_card("Qc",data.x,data.y)
			
			
if __name__ == "__main__":
	Controller()
