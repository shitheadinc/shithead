# -*- coding: iso-8859-1 -*-

# Erster Ansatz zur Entwicklung eines Kartenspiels
# Ein Kartenstapel wird generiert und ein Spieler erhält
# aus diesem Stapel 5 Karten. Er kann anschließend eine der Karten
# auswählen und ablegen. Dann wird geprüft, ob diese Karte gelegt werden
# kann.

import sys
from random import shuffle
from output import PrototypeGUI

###global vars:
hand_size = 2

class Player:
	def __init__(self, game_state, index):
		self.player_index = index # wird nur f\"ur output benutzt
		self.hand = [] # Hand des Spielers
		self.down_cards = [] # je 1 stapel fuer jeden spieler 
		self.up_cards = [] # je 1 stapel fuer jeden spieler 
		self.game_state = game_state
		return

	def give_hand_cards(self,new_cards):
		"""Der Spieler nimmt Handkarten auf"""
		self.hand.extend(new_cards)
		return

	def give_table_cards(self,up_cards,down_cards):
		"""Der Spieler erhaelt Tischkarten (up- and down-cards) vom Stack. Die Groesse entspricht normalerweise jeweils der maximalen Handkartenanzahl."""
		self.down_cards.extend(down_cards)
		self.up_cards.extend(up_cards)
		return

	def play_card(self):
		"""Der Spieler spielt Handkarten aus"""
		print " Player " + str(self.player_index) + "\'s turn."
		
		chosen_cards = self.choose_card() # choose a card
		
		if chosen_cards == -1:# check, if the user wants to/has to take the whole stack "schluck du specht!"
			return "i'll take 'em all"# beliebiger rueckgabewert..koennte auch '-1' bleiben
		#gui.display_cards(chosen_cards)
		
		for c in chosen_cards:
			self.hand.remove(c) # give away the card
		
		#print " New Hand of Player " + str(self.player_index)
		#gui.display_cards(self.hand)
		return chosen_cards

	def set_up_upcards(self):
		""" Wähle unter den Hand und Up-Cards neue Up-Cards. Die Anzahl entspricht der Handgröße."""
		choose_among = self.hand + self.up_cards
		print " Choose among this cards:"
		gui.display_cards(choose_among)
		chosen_cards = self.choose_card() # choose a card
		

	def choose_up_card(self):
		"""Nehme eine oder mehrere Karten der Up-Karten auf."""
		ret = []
		while True: # ask till we got a valid input
			indices = []
			try: # check for ValueError
				n = gui.select_hand_cards() # see gui file#
			except:
				print("> No valid input")
			
			if not n: # check for empty list
				print("> No input")
				continue
			
			for i in n:
				if (0 <= i < len(self.up_cards)) & (i not in indices):
					indices.append(i) # only append valid card-indices
				else:
					indices = [] # continue loop and ask for next input
					break
			
			if not indices: # dont take empty lists:
				print("> No valid input")
				continue
			
			ret = [self.up_cards[i] for i in indices] # get cards from the list of indices
			if  (len(ret) == 1) | Rules.same_valued(ret): 
				break # output is valid, break endless-loop
			else:
				print("> No valid choice")
		return ret


	def choose_card(self):
		"""Zum auswaehlen einer Karte von der Hand faengt ungueltige Zahlen ab"""
		ret = [] # collect return values (cards)
		
		while True: # ask till we got a valid input
			indices = [] #clean list after bad input
			try: # check for ValueError
				n = gui.select_hand_cards() # see gui file#
			except:
				print("> No valid input")
			if not n: # check for empty list
				print("> No input")
				continue
			
			if n == [-1]:# check, if the user wants to/has to take the whole stack
				ret = -1
				break
			
			for i in n:
				if (0 <= i < len(self.hand)) & (i not in indices): #only take values once
					indices.append(i) # only append valid card-indices
				else:
					indices = [] # continue loop and ask for next input
					break
			if not indices: # dont take empty lists:
				print("> No valid input")
				continue
			ret = [self.hand[i] for i in indices] # get cards from the list of indices
			# only take same-valued cards
			if Rules.check_allowed(self.game_state,ret) & \
				((len(ret) == 1) | Rules.same_valued(ret)): 
				break # output is valid, break endless-loop
			else:
				print("> No valid choice")
		return ret

# Diese Klasse enthält die Checks, welche im Sinne der Shithead-Regeln durchgeführt werden.
# Es empfiehlt sich, die Methoden durch den Decorator "@staticmethod" static zu machenh
class Rules: #### TODO Diese Klasse mit der GameState-Klasse mergen
	@staticmethod
	def same_valued(cards):
		"""pruefe, ob in der liste 'cards' von karten,
		alle karten den selben value haben. returns bool"""
		if not cards:
			return False;
		values = [c[0] for c in cards] # retrieve values for each card
		#if values[1:] == values[:-1]:
		if values.count(values[0]) == len(values):
			return True
		else:
			return False

	@staticmethod
	def check_for_restart(open_cards):
		"""checke die top 4 cards für einen Restart (die 4 obersten Karten sind same-valued)"""
		if len(open_cards) < 4:
			return False
		if Rules.same_valued(open_cards[-4:]):
			return True
		return False

	@staticmethod
	def game_is_won_by(player):
		"""no card left to draw? => win"""
		if (not player.hand) & (not player.up_cards) & (not player.down_cards): 
			return True
		else:
			return False #default

	@staticmethod
	def check_allowed(game_state,list_of_cards):
		"""pruefe ob eine menge von karten auf den 'stack' gelegt werden darf"""
		#### For DEBUG:
		#return False # skip this check
		if type(list_of_cards) is list: # Argument koennte Liste oder Tupel (Card) sein
			card = list_of_cards[0]
		else:
			card = list_of_cards
			
		if not game_state.open_cards:
			return True # Wenn die Liste leer ist, kann alles gelegt werden
		top = game_state.open_cards[-1] # letzte Karte aus dem Stapel (d.h.: oberste)

		if card[0]==1 or card[0]==2: # Zweien und Dreien gehen immer
			return True
		elif top[0] == 8: # Unter 9
			return card[0] <= 8
		elif top[0] > 2: # keine 2 oder 3
			return card[0] >= top[0]
		elif top[0] == 2: # Unsichtbar-Karte
			depth = 2
			# solange die nächste Karte betrachten, bis keine 3 mehr:
			while depth<len(game.gstate.open_cards) \
				and game_state.open_cards[-depth][0] == 2:
				depth += 1
			if depth >= len(game_state.open_cards): # es liegen nur dreien im Stapel
				return True # dann kann alles gelegt werden
			first_diff = game_state.open_cards[-depth] # erste Karte, die nicht 3 ist
			if first_diff[0]==8:
				return card[0] <= 8
			else:
				return card[0] >= first_diff[0]
		else:
			return False
			#raise ValueError("Something is wrong here: %s" % top[0])

class GameState:
	""" Diese Klasse beinhaltet den aktuellen Status in Form von Variablen. Alle Methoden, die den Status verändern werden ebenfalls in dieser Klasse oder der SpielerKlasse implementiert. Es ist geplant, dass diese Klasse wiederum Instanzen einer Spielerklasse beinhaltet. Die Trennung wird vorgenommen, sodas ein Spieler nur seine eigene Hand modifizieren kann."""
	def __init__(self):
		self.turn = 0 # initiale spielrunde
		self.is_finished = False
		self.deck = []# Liste der Karten/ ziehstapel
		
		self.players = [Player(self,1), Player(self,2)] # zwei spieler
		self.open_cards = [] # ablagestapel /stack
		self.trash_stack = [] # muellstapel
		self.active_player_index = 0 # spieler 1 faengt an
		return

	def build_and_shuffle_deck(self):
		"""build and shuffle a deck using the 'self.' variables"""
		color = [1,2,3,4]
		value = [1,2,3,4,5,6,7,8,9,10,11,12,13] # 1=2,2=3,...,13=Ass
		
		# Initialisiere Karten und :
		for v in value:
			for c in color:
				self.deck.append((v,c))
		
		shuffle(self.deck) # Deck mischen
		return

	def give_initial_hand_cards(self):
		"""give hand cards for each player, doesnt contain checks!"""
		# 5 Karten auf die Spielerhände:
		global hand_size
		
		for player in self.players:
			hand = []
			for i in range(hand_size):
				hand.append( self.deck.pop() )
			
			print " Hand " + str(player.player_index) + ":"
			gui.display_cards(hand)
			
			player.give_hand_cards( hand )
		return

	def give_initial_table_cards(self):
		global hand_size
		
		for player in self.players:
			up_cards = []
			down_cards = []
			for i in range(hand_size):
				up_cards.append(self.deck.pop())
			for i in range(hand_size):
				down_cards.append(self.deck.pop())
			player.give_table_cards(up_cards,down_cards)
			

	def draw_cards(self):
		"""draw deck-cards until you reach your hand card size or the deck runs out of cards."""
		global hand_size
		player = self.players[self.active_player_index] # current player
		if self.deck: # zuerst wird das Deck leer gemacht
			while (len(player.hand) < hand_size) & (self.deck != []):  #draw till max.
				player.give_hand_cards( [self.deck.pop()] )
		
		elif (len(player.up_cards) != 0)  & (len(player.hand) == 0): # nur ziehen, wenn karten da und hand leer
			users_choice = player.choose_up_card() # TODO der user darf die karte selbst aussuchen
			for c in users_choice:
				player.up_cards.remove(c)
			player.give_hand_cards( users_choice )
		
		elif (len(player.down_cards) != 0) & (len(player.hand)  == 0):# nur ziehen, wenn karten da und hand leer
			player.give_hand_cards( [player.down_cards.pop()] )
		
		if Rules.game_is_won_by(player):# siegbedingung checken
			print "Player " + str(player.player_index) + " won the game, he has no cards left!\t[Congratulations, fucker]"
			sys.exit()
		return

	def activate_card_effect(self,chosen_cards): # TODO Diese Methode sollte eigentlich in "Rules"
		"""Verwaltet, wie welche Karte wirkt ('7' bedeutet aussetzen etc). Sollte eigentlich der
		ShitheadRules-Klasse angehoeren, ist aber auch hier nicht verkehrt.."""
		if type(chosen_cards) is list: # Argument koennte Liste oder Tupel (Card) sein
			card = chosen_cards[0]
		else:
			card = chosen_cards
		
		if card == "i'll take 'em all": #### take all
			print "take all"
			self.players[self.active_player_index].give_hand_cards(self.open_cards)
			self.open_cards = []
			gui.display_top_card(self.open_cards)
			return
			
		elif card[0] == 1: #### 2 restart
			self.stack_restart()
			gui.display_top_card(self.open_cards)
			return
			
		elif card[0] == 6: #### 7 again
			self.next_turn() # ok fuer beliebige Anzahl von Spielern!
			self.add_cards_to_stack(chosen_cards)

		else: #### 3,9,4,5,6,8,10,B,D,K,A
			#print "something played"
			self.add_cards_to_stack(chosen_cards)
		
		#### check for restart after 4-of-the-same-kind TODO shortcut: if 4 cards are played
		if Rules.check_for_restart(self.open_cards):
			self.stack_restart()
		
		gui.display_top_card(self.open_cards)
		return

	def add_cards_to_stack(self,chosen_cards):
		"""just extends the stack with some given cards"""
		self.open_cards.extend(chosen_cards)
		return

	def stack_restart(self):# neustart (nach 2 oder komplettierung zu 4
		"""wende restart-Shithead-Regel an"""
		self.trash_stack.extend(self.open_cards) #### back up cards
		self.open_cards = []
		self.next_turn() # assumption: 2 Spieler-Spiel
		return

	def next_player(self):
		"""Berechne den Index des aktuellen Spielers"""
		self.active_player_index = self.turn % len(self.players)
		return self.active_player_index

	def next_turn(self):
		"""simply increments the turn-counter"""
		self.turn += 1
		return


	def print_status(self):
		"""DEBUG function für die Konsole"""
		for player in self.players:	
			print "\n The Hand of player " + str(player.player_index) + " consists of"
			gui.display_cards(player.hand)
			print " He also has " + str(len(player.down_cards)) + " Down-Cards and the following Up-Cards:"
			gui.display_cards(player.up_cards)
		return

class Game:
	def __init__(self):
		print "\n\nWelcome, shithead!\n> Initializing Stuff..\n"
		self.gstate = GameState()

	def start_game(self):
		"""Verteile Startkarten und starte die (endlos) Spielsschleife. Das Programm endet, sobald ein Spieler gewonnen hat."""
		self.gstate.build_and_shuffle_deck()
		
		self.gstate.give_initial_hand_cards()
		self.gstate.give_initial_table_cards()
		
		self.gstate.deck = self.gstate.deck[-3:] ## DEBUG TODO bitte auskommentieren
		
		##### GAME LOOP ####
		while True:
			current_player = self.gstate.players[self.gstate.next_player()] # switch between players
			
			chosen_cards = current_player.play_card() # players choice
			
			self.gstate.activate_card_effect(chosen_cards) # handle cards
			
			self.gstate.draw_cards()
			
			self.gstate.print_status()# DEBUG: print hands
			
			print "\n------------------------------------------------\n"
			self.gstate.next_turn() #increment turn variable

####################### starting game, setting up properties####################
if __name__ == "__main__":
	# instanziiere GUI:
	gui = PrototypeGUI()

	# starte Spiel
	game = Game() 
	game.start_game()