# coding: utf-8
# About Dialog for the Shithead Game

from gi.repository import Gtk

class AboutDialog(Gtk.AboutDialog):
	
	def __init__(self,parent):
		Gtk.AboutDialog.__init__(self)
		
		# Hauptfenster ist blockiert, wenn Dialog offen ist:
		self.set_transient_for(parent)
		# Dialog wird auf dem Hauptfenster zentriert:
		self.set_position(Gtk.WindowPosition.CENTER_ON_PARENT)
		
		
		self.set_program_name("Shithead")
		self.set_version("0.1")
		self.set_copyright("Copyright © LordLattenjub and Prxxxe")
		self.set_comments("A basic Shithead game.")
		self.set_website("https://bitbucket.org/shitheadinc/shithead")
		self.set_website_label("https://bitbucket.org/shitheadinc/shithead")
		self.run()
		self.destroy()
