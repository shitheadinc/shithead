# KONSTANTEN
# hier sind einige wichtige Konstanten fuer die Darstellung auf
# der Clutter-Stage gespeichert.

# Randbreiten:
x_rand = 10
y_rand = 10

# Karten:
x_card = 72 	# Kartenbreite
y_card = 96		# Kartenhoehe

# Verschiebung zwischen zwei Karten:
x_offset = 15
y_offset = 28

# Minimalgroesse des Fensters um vernuenftige 
# Darstellung zu gewaehrleisten:
min_width = 2*x_rand + x_card + 50*x_offset
min_height = 6*y_rand + 5*y_card + 2*y_offset

