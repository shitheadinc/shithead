from gi.repository import Gtk,GtkClutter
from gi.repository import Clutter

from Constants import *
import sys
import os

import AboutDialog


class MainWindow(Gtk.Window):
	
	# KONSTRUKTOR
	# erzeugt das Fenster und registriert die uebergebene
	# Listener-Funktion on_event.
	def __init__(self, on_event):
		Gtk.Window.__init__(self, title="Shithead")
		
		# window at center of screen:
		self.set_position(Gtk.WindowPosition.CENTER)
		# quit program when closing window:
		self.connect("delete-event", Gtk.main_quit) # close event
		
		# Den absoluten Pfad zum Base-Dir (das wo die Ordner src und img sind)
		# herausfinden und speichern, damit Texturen auch angezeigt werden, wenn
		# die Datei aus einem beliebigen Ordner ausgefuehrt wird.
		appdir = os.path.abspath(os.path.dirname(__file__))
		self.basedir = os.path.abspath(os.path.join(appdir, os.pardir, os.pardir))
		print self.basedir
		
		
		self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=5)
		self.add(self.vbox)
		
		# Menu Bar erzeugen und hinzufuegen:
		self.menubar = self.create_menubar()
		self.vbox.pack_start(self.menubar, False, False, 0)
		
		# Clutter-Stage in Gtk einbinden:
		GtkClutter.init([])
		self.clutter_widget = GtkClutter.Embed.new()
		self.clutter_widget.connect('configure-event', self.on_window_resized)
		# muss request sein, damit kein 0x0-Fenster raus kommt:
		#self.clutter_widget.set_size_request(1200,800)
		self.stage = self.clutter_widget.get_stage()
		self.vbox.add(self.clutter_widget)
		
		# Stage gruen machen:
		green = Clutter.Color.new(3,55,30,255) # red, green, blue, alpha
		self.stage.set_color(green)
		self.stage.set_name("STAGE")
		# Listener fuer CLicks auf die Stage:
		#self.stage.connect('button-press-event', self.on_stage_button_press)
		self.stage.connect('button-press-event', on_event)
		
		# Dictionary aus Name-Texture-Paaren
		# hier werden die Karten, die sich auf der Stage befinden drin gespeichert
		# um einfach auf die Texturen zugreifen zu koennen:
		self.card_textures = {} 
		
		# Minimal-Groesse setzen:
		self.resize(min_width,min_height)
		self.clutter_widget.set_size_request(min_width,min_height)
		
		# Listener setzen:
		self.register_listener(on_event)
		
	
	##############################################
	### METHODEN ZUR ERZEUGUNG DES FENSTERS
	##############################################
	
	
	def create_menubar(self):	
		### CREATE ACTION GROUP ###
		actiongroup = Gtk.ActionGroup("my_actions")
	
		# Game Menu:
		action_gamemenu = Gtk.Action("GameMenu", "Game", None, None)
		self.action_new = Gtk.Action("NEW_GAME", "New Game", None, Gtk.STOCK_NEW) # braucht connect
		action_quit = Gtk.Action("QUIT", "Quit", None, Gtk.STOCK_QUIT)
		action_quit.connect("activate", Gtk.main_quit, None) # Programm beenden
		actiongroup.add_action(action_gamemenu)
		actiongroup.add_action(self.action_new)
		actiongroup.add_action(action_quit)
	
		# Help Menu:
		action_helpmenu = Gtk.Action("HelpMenu", "Help", None, None)
		action_about = Gtk.Action("About", "About", None, Gtk.STOCK_ABOUT)
		action_about.connect("activate", self.on_about)
		actiongroup.add_action(action_helpmenu)
		actiongroup.add_action(action_about)		
		
		### CREATE UIMANAGER ###
		# Der UI-Manager wird aus dem verwendeten XML-Dokument erzeugt.
		# Er verwaltet aussehen und Gliederung des User-Interfaces.
		
		uimanager = Gtk.UIManager()
		try:
			uimanager.add_ui_from_file(self.basedir+"/src/view/MenuBar.ui")
		except:
			print "file not found"
			sys.exit()
		
		# add action group to ui manager:
		uimanager.insert_action_group(actiongroup)
	
		# create menubar from ui-manager / xml:
		menubar = uimanager.get_widget("/MenuBar")
		return menubar
	
	
	##############################################
	### METHODEN ZUR KOMMUNIKATION MIT CONTROLLER
	### UND VIEW
	##############################################
	
	def register_listener(self,listener_func):
		self.action_new.connect("activate", listener_func)
	
	
	##############################################
	### METHODEN FUER DIE BEWEGUNG DER KARTEN
	##############################################
	
	
	
	# Positioniert eine Karte an der gewuenschten Position auf der
	# Stage. Karten fliegen vom Koordinatenursprung ins Bild
	# cardname: Name der Karte, z.B. As,Th,7d
	# xpos: x-Position in Pixeln
	# ypos: y-Position in Pixeln
	def place_card(self,cardname,xpos,ypos):
		filename = self.basedir+"/img/cards/"+str(cardname)+".png"
		card = Clutter.Texture.new_from_file(filename)
		# initiale Kartenpos vor der Animation ist der Ursprung:
		card.set_position(0,0)
		card.animatev(Clutter.AnimationMode.EASE_OUT_BOUNCE, 500, ["x","y"], [xpos,ypos])
		self.stage.add_actor(card)
		self.card_textures[cardname] = card # Karte dem Dictionary hinzufuegen
		
	# Eine auf dem Canvas befindliche Karte bewegt sich an die
	# vorgegebene Position
	def move_card(self,cardname,xpos,ypos):
		if cardname not in self.card_textures:
			print "Karte nicht vorhanden"
			return
		card = self.card_textures[cardname]
		animation = card.animatev(Clutter.AnimationMode.LINEAR, 500, ["x","y"], [xpos,ypos])
		return
		
	# Die Karten des Spielers werden in der unteren linken
	# Ecke des Spielfeldes positioniert:
	def place_player_hand(self,cardnames):
		xpos = x_rand
		ypos = self.stage.get_height() - y_rand - y_card
		for cardname in cardnames:
			self.place_card(cardname,xpos,ypos)
			xpos += x_offset
	
	# Die Karten des Gegners werden in der oberen rechten
	# Ecke des Spielfeldes positioniert:
	def place_villain_hand(self,num_cards):
		if num_cards>=1:
			xpos = self.stage.get_width() - x_rand - x_card
			ypos = y_rand
			for i in range(num_cards):
				self.place_card("back",xpos,ypos)
				xpos -= x_offset
				
	# Hinterlegt die momentan ausgewaehlte Karte rot:
	def place_selector(self):
		xpos = x_rand
		ypos = self.stage.get_height() - y_rand - y_card
		transp_red = Clutter.Color.new(180,40,40,100)
		selector = Clutter.Rectangle.new_with_color(transp_red)
		selector.set_size(x_offset,y_card)
		selector.set_position(xpos,ypos)
		self.stage.add_actor(selector)
		
	##########################################
	#### LISTENER
	##########################################	
	
	# Opens the applications About Dialog:
	def on_about(self,widget):
		AboutDialog.AboutDialog(self)
		
	# Stage wird mit verkleinert, wenn das Window ge-resized wird.
	# Aber: Minimum-Groesse der Stage muss eingehalten werden!
	def on_window_resized(self,widget=None,event=None,a=0):
		(width,height) = self.get_stage_size()
		print str((width,height))
		if width<min_width or height<min_height:
			self.clutter_widget.set_size_request(min_width,min_height)
	
	##############################################
	### BERECHNUNG RELATIVER POSITIONEN
	##############################################
	
	def get_stage_size(self):
		return self.stage.get_size()
		
	
##############################################
### START DES PROGRAMMS HIER
##############################################	

def test(widget):
	name = widget.get_name()
	print "Event empfangen von %s" % name

if __name__ == "__main__":
	win = MainWindow(test)
	win.place_card("Qc",145,275)
	win.place_card("Kc",145,275+y_offset)
	print "Initiale Stage-Size: "+str(win.get_stage_size()) # Vor show_all ist die Stage-Size Unsinn!
	win.show_all()
	win.place_player_hand(["2d","2c","2h","4s"])
	win.place_villain_hand(13)
	win.place_selector()
	print "Stage-Size nach show_all: "+str(win.get_stage_size())
	Gtk.main()


